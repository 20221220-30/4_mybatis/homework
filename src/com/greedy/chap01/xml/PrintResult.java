package com.greedy.chap01.xml;

import java.util.List;

public class PrintResult {


	public static void printLoginList(List<LoginDTO> loginList) {
		for(LoginDTO login : loginList) {
			System.out.println(login);
		}
	}
	
	
	public void printLogin(LoginDTO login) {
		System.out.println(login);
	}

	public static void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "회원 조회에 실패하였습니다."; break;
		case "selectOne" : errorMessage = "메뉴 조회에 실패하였습니다."; break;
		case "insert" : errorMessage = "메뉴 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "메뉴 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "메뉴 삭제에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}
	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "메뉴 등록에 성공하였습니다."; break;
		case "update" : successMessage = "메뉴 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "메뉴 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
		

}
}