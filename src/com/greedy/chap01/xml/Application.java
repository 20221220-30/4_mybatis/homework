package com.greedy.chap01.xml;


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Application {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		LoginController loginController = new LoginController();
		
		
		do {
			System.out.println("===== 메뉴 ======");
			System.out.println("1. 전체 회원 조회 ");
			System.out.println("2. 로그인 ");
			System.out.println("3. 회원가입");
			System.out.println("4. 계정 수정");
			System.out.println("5. 계정 삭제");
			System.out.println("6. 지정 계정 변경");
			System.out.print(" 원하시는 메뉴를 입력 : ");
			int no = sc.nextInt();
			
			
			switch(no) {
			case 1: loginController.selectAllMenu(); break;
			case 2: loginController.selectLoginById(inputLoginId());break;
			case 3: loginController.registlogin(inputLogin()); break;
			case 4: loginController.modifyLogin(inputModifyLogin());break;
			case 5: loginController.deleteLogin(inputLoginNo()); break;
			case 6: trimSubLogin(); break;
			default: System.out.println("잘못 된 메뉴를 선택하셨습니다.");
			}
		}while(true);
		
}

	

	private static void trimSubLogin() {
		
		Scanner sc = new Scanner(System.in);
		LoginService loginService = new LoginService();
		
		do {
			System.out.println("====유저 계정 변경/수정======");
			System.out.println("1. 계정 수정");
			System.out.println("9. 이전 메뉴로");
			int no = sc.nextInt();
			
			switch(no) {
			case 1 : loginService.modifyLogin(inputChangeInfo()); break;
			case 9 : return;	
			}
			
		}while(true);
	}



	private static Map<String, Object> inputChangeInfo() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("변경할 계정 아이디 입력 : ");
		int no = sc.nextInt();
		System.out.println("변경할 이메일 입력 : ");
		sc.nextLine();
		String email = sc.nextLine();
		System.out.println("변경할 나이 입력 : ");
		int age = sc.nextInt();
		
		
		Map<String, Object> criteria= new HashMap<>();
		criteria.put("no", no);
		criteria.put("email", email);
		criteria.put("age", age);
		
		return criteria;
	}



	private static Map<String, String> inputLoginId() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("아이디 입력 : ");
		String id = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("id", id);
		
		
		return parameter;
	}
	private static Map<String, String> inputLogin() {

		Scanner sc = new Scanner(System.in);
		System.out.println("아이디 : ");
		String id = sc.nextLine();
		System.out.println("비밀번호 : ");
		String pwd = sc.nextLine();
		System.out.println("이름 : ");
		String name = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("id", id);
		parameter.put("pwd", pwd);
		parameter.put("name", name);
		
		return parameter;
	}

	
	private static Map<String, String> inputModifyLogin() {
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("수정할 회원 번호 : ");
		String no = sc.nextLine();
		System.out.println("수정할 비밀번호 : ");
		String pwd = sc.nextLine();	
		System.out.println("수정할 이메일 : ");
		String email = sc.nextLine();
		System.out.println("수정할 전화번호 : ");
		String phone = sc.nextLine();
		System.out.println("수정할 나이 : ");
		String age = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("no", no);
		parameter.put("pwd", pwd);
		parameter.put("email", email);
		parameter.put("phone", phone);
		parameter.put("age", age);
		
		return parameter;
	}

		

		
	private static Map<String, String> inputLoginNo() {
		Scanner sc = new Scanner(System.in);
		System.out.print("회원번호 입력 : ");
		String no = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("no", no);
		
		
		return parameter;
		
	}


		
		
		
		
		
		
		
		
		
		
		
	

}
