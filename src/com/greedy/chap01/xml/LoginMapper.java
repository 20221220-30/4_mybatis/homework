package com.greedy.chap01.xml;

import java.util.List;

public interface LoginMapper {
	
	
	List<LoginDTO> selectAllLogin();

	LoginDTO selectLoginById(int id);

	int insertLogin(LoginDTO login);

	int updateLogin(LoginDTO login);

	int deleteLogin(int no);




	
	

}
