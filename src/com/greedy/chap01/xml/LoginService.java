package com.greedy.chap01.xml;

import java.util.List;
import java.util.Map;

import static com.greedy.chap01.xml.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

public class LoginService {
	
	private LoginMapper loginMapper;
	private DynamicSqlMapper mapper;
	
	
	
	public List<LoginDTO> selectAllLogin(){
	
	SqlSession sqlSession = getSqlSession();
	mapper = sqlSession.getMapper(DynamicSqlMapper.class);

	
	loginMapper = sqlSession.getMapper(LoginMapper.class);
	List<LoginDTO> loginList = loginMapper.selectAllLogin();
	
	sqlSession.close();
	
	return loginList;
	
}


	public LoginDTO selectLoginById(int id) {
		
		SqlSession sqlSession = getSqlSession();
		loginMapper = sqlSession.getMapper(LoginMapper.class);
		
		LoginDTO login = loginMapper.selectLoginById(id);
		
		sqlSession.close();
		
		return login;
	}


	public boolean registLogin(LoginDTO login) {
		
		SqlSession sqlSession = getSqlSession();
		loginMapper = sqlSession.getMapper(LoginMapper.class);
		
		int result = loginMapper.insertLogin(login);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}

		sqlSession.close();
		
		return result > 0 ? true : false;
	}


	public boolean modifyLogin(LoginDTO login) {
		SqlSession sqlSession = getSqlSession();
		loginMapper = sqlSession.getMapper(LoginMapper.class);
		
		int result = loginMapper.updateLogin(login);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		sqlSession.close();
		return result > 0 ? true : false;
	}


	public boolean deleteLogin(int no) {
		SqlSession sqlSession = getSqlSession();
		loginMapper = sqlSession.getMapper(LoginMapper.class);
		
		int result = loginMapper.deleteLogin(no);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		sqlSession.close();
		return result > 0 ? true : false;
	}


	public void modifyLogin(Map<String, Object> changeInfo) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(DynamicSqlMapper.class);
		
		int result = mapper.modifyLogin(changeInfo);
		
		if(result > 0) {
			sqlSession.commit();
			System.out.println("계정 정보 변경에 성공하였습니다. ");
		}else {
			sqlSession.rollback();
			System.out.println("메뉴 정보 변경에 실패하였습니다.");
		}
		
		sqlSession.close();
	}


	




	


	

}

