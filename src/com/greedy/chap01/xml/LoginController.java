package com.greedy.chap01.xml;

import java.util.List;
import java.util.Map;


public class LoginController {

	private final LoginService loginService;
	private final PrintResult printResult;
	
	public LoginController() {
		loginService = new LoginService();
		printResult = new PrintResult();
		
	}
public void selectAllMenu() {
		
		List<LoginDTO> loginList = loginService.selectAllLogin();
		
		if(loginList != null) {
			PrintResult.printLoginList(loginList);
		}else {
			PrintResult.printErrorMessage("selectList");		
	}


}

public void selectLoginById(Map<String, String> parameter) {

	int id = Integer.parseInt(parameter.get("id"));
	
	LoginDTO login = loginService.selectLoginById(id);
	
	if(login !=null) {
		printResult.printLogin(login);
		
	}else {
		printResult.printErrorMessage("selectOne");
	}
}
	
public void registlogin(Map<String, String> parameter) {
	LoginDTO login = new LoginDTO();
	login.setLoginId(Integer.parseInt(parameter.get("id")));
	login.setLoginPwd(parameter.get("pwd"));
	login.setUserName(parameter.get("name"));

	if(loginService.registLogin(login)) {
		printResult.printSuccessMessage("intsert");
	}else {
		printResult.printErrorMessage("insert");
	}
	
}

public void modifyLogin(Map<String, String> parameter) {
	
	LoginDTO login = new LoginDTO();
	login.setLoginNo(Integer.parseInt(parameter.get("no")));
	login.setLoginPwd(parameter.get("pwd"));
	login.setEmail(parameter.get("email"));
	login.setPhone(parameter.get("phone"));
	login.setAge(Integer.parseInt(parameter.get("age")));
	
	
	if(loginService.modifyLogin(login)) {
		printResult.printSuccessMessage("update");
	}else {
		printResult.printErrorMessage("update");
	}
}
public void deleteLogin(Map<String, String> parameter) {
	
	int no = Integer.parseInt(parameter.get("no"));
	
	if(loginService.deleteLogin(no)) {
		printResult.printSuccessMessage("delete");
	}else {
		printResult.printErrorMessage("delete");
	}
	
}


}


