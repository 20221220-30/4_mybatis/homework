package com.greedy.chap01.xml;

import java.sql.Date;

public class LoginDTO {
	
	private int loginNo;
	private int loginId;
	private String loginPwd;
	private String userName;
	private String gender;
	private String email;
	private String phone;
	private int age;
	private Date enrollDate;
	
	public LoginDTO() {}

	public LoginDTO(int loginNo, int loginId, String loginPwd, String userName, String gender, String email,
			String phone, int age, Date enrollDate) {
		super();
		this.loginNo = loginNo;
		this.loginId = loginId;
		this.loginPwd = loginPwd;
		this.userName = userName;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.age = age;
		this.enrollDate = enrollDate;
	}

	public int getLoginNo() {
		return loginNo;
	}

	public void setLoginNo(int loginNo) {
		this.loginNo = loginNo;
	}

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	@Override
	public String toString() {
		return "LoginDTO [loginNo=" + loginNo + ", loginId=" + loginId + ", loginPwd=" + loginPwd + ", userName="
				+ userName + ", gender=" + gender + ", email=" + email + ", phone=" + phone + ", age=" + age
				+ ", enrollDate=" + enrollDate + "]";
	}



}